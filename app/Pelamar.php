<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelamar extends Model
{
    protected $table = 'data_pelamar';

    protected $fillable = [
        'nama_lengkap', 'nama_panggilan', 'tempat_lahir','tanggal_lahir',
        //'golongan_darah',
        'nomor_identitas',
        //'jenis_kelamin',
         'tinggi_badan', 'berat_badan',
        //'status_perkawinan',
         'jalan'// 'provinsi',
       // 'kota',
        'nomor_telepon', 'email'
    ];

    /*protected $hidden = [
        'password', 'remember_token',
    ];*/
}
