<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class PelamarController extends Controller
{

    public function store(Request $request)
    {
        $data = [
            'nama_lengkap'=> $request->nama_lengkap,
            'nama_panggilan'=> $request->nama_panggilan,
            'tempat_lahir'=> $request->tempat_lahir,
            'tanggal_lahir'=> $request->tanggal_lahir,
            //'golongan_darah'=> $request->golongan_darah,
            'nomor_identitas'=> $request->nomor_identitas,
           // 'jenis_kelamin'=> $request->jenis_kelamin,
            'tinggi_badan'=> $request->tinggi_badan,
            'berat_badan'=> $request->berat_badan,
            //'status_perkawinan'=> $request->status_perkawinan,
            'jalan'=> $request->jalan,
           // 'provinsi'=> $request->provinsi,
            'nomor_telpon'=> $request->nomor_telpon,
            'email'=> $request->email
        ];


        $input = DB::table('data_pelamar')->insert($data);

        return redirect('admin/employee/create');
    }


}
