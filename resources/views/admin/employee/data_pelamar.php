extends('admin.layout.app')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">FORM RECRUITMENT</div>
                <div class="panel-footer">
                    <a href="{{route('admin.employee.create')}}" class="btn btn-success">Add FORM</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection