@extends('admin.layout.app')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Pengisian Data Diri</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ url('/post/pelamar')}}">
                        <input  type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Nama Panggilan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_panggilan" name="nama_panggilan" placeholder="Nama Panggilan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Tempat Lahir</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir">
                                </div>
                            </div>
                                <div class="form-group" >
                                    <label for=""  class="col-sm-2 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-10 form-control-static"  >
                                        <input name="tanggal_lahir" type="date" class="form-control" >
                                    </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">AGAMA</label>
                                <div class="col-sm-10">
                                    <select name=Agama class="form-control">
                                        <option name=Thn>Agama
                                        <option name=Thn>ISLAM
                                        <option name=Thn>KRISTEN PROTESTAN
                                        <option name=Thn>KRISTEN KATOLIK
                                        <option name=Thn>HINDU
                                        <option name=Thn>BUDDHA
                                        <option name=Thn>KONG FU CU
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">GOLONGAN DARAH </label>
                                <div class="col-sm-10">
                                    <select name=golongan_darah class="form-control">
                                        <option name=Thn>Golongan darah
                                        <option name=Thn>A
                                        <option name=Thn>B
                                        <option name=Thn>AB
                                        <option name=Thn>O
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Nomor Indentitas</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_identitas" name="nomor_identitas" placeholder="(KTP/SIM)">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Jenis Kelamin</label>
                                <div class="col-sm-10">
                            <input type="checkbox" name="laki-laki" value="Laki-laki">Laki-laki <br/>
                            <input type="checkbox" name="perempuan" value="Perempuan">Perempuan  <br/>
                                    </div>
                            </div>
                           <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Tinggi badan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="tinggi_badan" name="tinggi_badan" placeholder="Tinggi Badan">
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Berat Badan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="berat_badan" name="berat_badan" placeholder="Berat Badan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Status Perkawinan</label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="belum_nikah" value="belum_nikah">Belum Nikah  <br/>
                                    <input type="checkbox" name="Nikah" value="Nikah">Nikah  <br/>
                                    <input type="checkbox" name="janda_duda" value="janda/duda">Janda/Duda  <br/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label" >Alamat </label>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Jalan </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="jalan" name="jalan" placeholder="Jalan">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Provinsi</label>
                                <div class="col-sm-10">
                                    <select name=provinsi class="form-control">
                                        <option name=>Provinsi
                                        <option name=>Jawa Barat
                                        <option name=>Jawa Tengah
                                        <option name=>Jawa Timur
                                        <option name=>Banten
                                        <option name=>DKI Jakarta
                                        <option name=>DI Yogyakarta
                                        <option name=>Sumatera Utara
                                        <option name=>Sumatera Barat
                                        <option name=>Sumatera Selatan
                                        <option name=>Sumatera Timur
                                        <option name=>Sumatera Tengah
                                        <option name=>Sumatera Utara
                                        <option name=>Provinsi Nanggroe Aceh Darussalam
                                        <option name=>Bengkulu
                                        <option name=>Riau
                                        <option name=>Kepulauan Riau
                                        <option name=>Jambi
                                        <option name=>Lampung
                                        <option name=>Bangka Belitung
                                        <option name=>Kalimantan Barat
                                        <option name=>Kalimantan Timur
                                        <option name=>Kalimantan Selatan
                                        <option name=>Kalimantan Tengah
                                        <option name=>Kalimantan Utara
                                        <option name=>Bali
                                        <option name=>Nusa Tenggara Timur
                                        <option name=>Nusa Tenggara Barat
                                        <option name=>Gorontalo
                                        <option name=>Sulawesi Barat
                                        <option name=>Sulawesi Tengah
                                        <option name=>Sulawesi Utara
                                        <option name=>Sulawesi Tenggara
                                        <option name=>Sulawesi Selatan
                                        <option name=>Maluku Utara
                                        <option name=>Maluku
                                        <option name=>Papua Barat
                                        <option name=>Papua ( Daerah Khusus )
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Kota</label>
                        </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Nomor Telpon </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_telpon" name="nomor_telpon" placeholder="Nomor Telpon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Email </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                            </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default">RESET</button>
                            <button type="submit" class="btn btn-info pull-right">Next</button>
                        </div>
                    </form>
                        <!-- /.box-footer -->
                </div>
            </div>
        </div>
    </section>
@endsection