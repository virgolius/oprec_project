<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

@include('admin.layout.header_script')

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

    @include('admin.layout.header')

    @include('admin.layout.sidebar')

    <div class="content-wrapper">
        @yield('content')
    </div>

    @include('admin.layout.footer')

</div>

@include('admin.layout.footer_script')

</body>
</html>
