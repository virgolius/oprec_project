<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel center-block">
            <span class="label label-default text-bold">{{strtoupper(Auth::user()->username)}}</span>
        </div>

        <ul class="sidebar-menu">
            <li class="header">NAVIGATION</li>
            <li class="{{ Request::is('admin/home') ? 'active' : '' }}"><a href="{{route('admin.home')}}"><i class="fa fa-home"></i> <span>Home</span></a></li>
            <li class="{{ Request::is('admin/employee','admin/employee/*')? 'active' : '' }}"><a href="{{route('admin.employee.index')}}"><i class="fa fa-user-o"></i> <span>Employee</span></a></li>
            <li class="{{ Request::is('admin/employee','admin/employee/*')? 'active' : '' }}"><a href="{{route('admin.employee.index')}}"><i class="fa fa-user-o"></i> <span>Data Pelamar</span></a></li>
        </ul>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
