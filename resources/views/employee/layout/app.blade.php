<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

@include('employee.layout.header_script')

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

    @include('employee.layout.header')

    @include('employee.layout.sidebar')

    <div class="content-wrapper">
        @yield('content')
    </div>

    @include('employee.layout.footer')

</div>

@include('employee.layout.footer_script')

</body>
</html>