<?php

use Illuminate\Database\Seeder;
use App\Admin;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'admin',
            'username' => 'admin',
            'password' => bcrypt('admin')
        ]);
    }
}
